const mongoose = require('mongoose');
const connection = mongoose.createConnection('mongodb://127.0.0.1:27017/test',{
    useNewUrlParser: true,
    useCreateIndex: true
});
const Schema = mongoose.Schema;

const autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(connection);

exports.mongoose = mongoose;
exports.connection = connection;
exports.Schema = Schema;
exports.autoIncrement = autoIncrement;