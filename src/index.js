const express = require('express');
const app = express();
require('./db/mongoose')
const Trades = require('./models/trades')
const utility = require('./common/utility')
const lodash = require('lodash');
const noRecordMsg = 'There are no trades in the given date range';
const mongoose = require('mongoose');

app.use(express.json());

app.delete('/erase', async (req, res) => {
    try {
        const trades = await Trades.deleteMany()

        if (!trades && trades.deletedCount == 0) {
            return res.status(404).send(utility.render_response(0, 'Something went wrong'))
        }else{
            res.send(utility.render_response(1, 'Records have been deleted'));
        }

        res.send(trades)
    } catch (e) {
        res.status(500).send();
    } 
});

app.post('/trades', (req, res) => {
    const trades = new Trades(req.body)

    trades.save().then(() => {
        res.status(201).send(utility.render_response(1, 'Record has been added'));
    }).catch((e) => {
        res.status(400).send(e)
    })
});

app.get('/trades', async (req, res) => {
    try{
        const resDoc = await Trades.find({},{}, { sort : {'_id' : 1} });

        resDoc.map((val, idx) => {
            val.id = val._id;
            delete val._id;

            return val;
        });

        res.send(utility.render_response(1, '', resDoc));
    }catch(e){
        res.status(500).send();
    }
});

app.get('/trades/users/:uid', async (req, res) => {
    try{
        if(req.params.uid){
            var uid = parseInt(req.params.uid);
            const resDoc = await Trades.find({'user.id' : uid},{}, { sort : {'_id' : 1} });

            if(resDoc.length > 0){
                res.send(utility.render_response(1, '', resDoc));
            }else{
                res.status(404).send();
            }
        }else{
            res.status(404).send();
        }
        
    }catch(e){
        res.status(500).send();
    }
});

app.get('/stocks/:symbol/trades', async (req, res) => {
    try{
        if(req.params.symbol){
            var filters = {'symbol' : req.params.symbol};
            var type = (req.query.type) ? req.query.type : '';
            var start = (req.query.start) ? req.query.start : '';
            var end = (req.query.end) ? req.query.end : '';
                      
            if(!lodash.isEmpty(type)){
                filters.type = lodash.toLower(type);
            }

            if(!lodash.isEmpty(start)){
                filters.timestamp = { $gte : new Date(start) };
            }

            if(!lodash.isEmpty(end)){
                filters.timestamp = { $lte : new Date(end) };
            }

            const resDoc = await Trades.find(filters,{symbol : 1, type : 1}, { sort : {'_id' : 1} });

            
            if(resDoc.length > 0){
                res.send(utility.render_response(1, '', resDoc));
            }else{
                res.status(404).send(utility.render_response(0,noRecordMsg));
            }
        }else{
            res.status(404).send(utility.render_response(0,noRecordMsg));
        }
        
    }catch(e){
        res.status(500).send();
    }
});

app.get('/stocks/:symbol/price', async (req, res) => {
    try{
        if(req.params.symbol){
            var filters = {'symbol' : req.params.symbol};
            var start = (req.query.start) ? req.query.start : '';
            var end = (req.query.end) ? req.query.end : '';
                      
            if(!lodash.isEmpty(start)){
                filters.timestamp = { $gte : new Date(start)};
            }

            if(!lodash.isEmpty(end)){
                filters.timestamp = { $lte : new Date(end)};
            }

            var agg = [
                {
                    $match : filters
                },
                {
                    $group : {
                        _id : '$symbol',
                        lowest : { $min : '$price' },
                        highest : { $max : '$price' }
                    }
                },
                {
                    $project : { _id : 1, lowest : 1, highest : 1}
                }
            ];

            const resDoc = await Trades.aggregate(agg);
            
            if(resDoc.length > 0){
                resDoc.forEach((v, k) => {
                    v.symbol = v._id;
                    delete v._id;
                });

                res.send(utility.render_response(1, '', resDoc));
            }else{
                res.status(404).send(utility.render_response(0,noRecordMsg));
            }
        }else{
            res.status(404).send(utility.render_response(0,noRecordMsg));
        }
        
    }catch(e){
        res.status(500).send();
    }
});

app.listen(3000);