const mongoose = require('./../db/mongoose')
const Schema = mongoose.Schema;
const connection = mongoose.connection;

const validator = require('validator')

const tradesScheme = new Schema({
    "type" : {
        type : String,
        enum : ["buy", "sell"],
        required : true
    },
    "user" : {
        type : Schema.Types.Mixed,
        required : true
    },
    "symbol" : {
        type : Schema.Types.Mixed,
        required : true
    },
    "shares" : {
        type : Number,
        required : true,
        min : 10,
        max : 30
    },
    "price" : {
        type : Number,
        required : true,
        min : 130.42,
        max : 195.65
    },
    "timestamp" : {
        type : Date,
        required : true
    }
});

tradesScheme.plugin(mongoose.autoIncrement.plugin, {
    model: 'Trades',
    field: '_id',
    startAt: 1,
    incrementBy: 1
});
const trades = connection.model('Trades', tradesScheme);


module.exports = trades