const lodash = require('lodash');

const render_response = (status, msg = '', jsonData = '') => {
    return retData = {
        'status' : (status) ? 'Success' : 'Error',
        'message' : msg,
        'data' : (!lodash.isEmpty(jsonData)) ? jsonData : {}
    };
}

module.exports.render_response = render_response;